#include <math.h>
#include "myformat.h"
#include "user.h"

/**************************************************
* Функция разбора строки на операторы и операнды  *
***************************************************/
int parse(char* str, char* cbase, char* numbers, char* operators)
{
    const char* copy = str;
    int z = 0,nextstate, bracket = 0, curstate = SPC;
    short int base = 0;
    do
    {
        switch (*copy)
        {
            case ' ': case '\n': case '\0':
                continue;
            case '(': case ')': 
                nextstate = SPC;
                base++;
                bracket++;
                break;
            case '+': case'-': case'*': case'/': case'%': case'^':
                nextstate = OPR;
                if(curstate == SPC || curstate == OPR)
                {
                    strncat(numbers, copy, 1);
                    z++;
                }
                else
                {
                    strncat(operators, copy, 1); 
                    strcat(numbers, ",");
                    z=0;      
                }
                break;
            case '0': case '1': case '2': case '3':
			case '4': case '5': case '6': case '7':
			case '8': case '9': case 'a': case 'b': 
            case 'c': case 'd': case 'e': case 'f': 
			case 'i': case 'j': case 'k': case 'l':
			case 'm': case 'n': case 'o': case 'p':
			case 'q': case 'r': case 's': case 't':
			case 'u': case 'v': case 'w': case 'x':
			case 'y': case 'z': case 'g': case 'h':
            case 'A': case 'B': case 'C': case 'D':
			case 'I': case 'J': case 'K': case 'L':
			case 'M': case 'N': case 'O': case 'P':
			case 'Q': case 'R': case 'S': case 'T':
			case 'U': case 'V': case 'W': case 'X':
			case 'Y': case 'Z': case 'G': case 'H':
            case 'E': case 'F':
            nextstate = DIG;
                if(base == 1)
                {
                    strncat(cbase, copy, 1); 
                    if ((strlen(cbase) > 2) && (bracket <= 1))
                    {
                        printf("Error, you didn't put a parenthesis\n");
                        return 2;
                    }
                }
                else
                    strncat(numbers, copy, 1);
                break;
            default:
                printf("Error, you entered an invalid character - %s\n", copy);
                return 1;
                break;
        }
        if(z > 1)
        {
            printf("Malformed expression!\n");
            exit(4);
        }
        curstate = nextstate;
    } while(*copy++);
    if(!*cbase)
        strcat(cbase, "10");
    return 0;
}

/*****************************************************************************
* Функция разбора строки, содержащей числа на отдельные числа, в виде строк  *
******************************************************************************/
int get_numbers(char str[], char *numbers[], char cbase[])
{
    int i = 0;
    char *res = strtok(str, ",");
    while (res)
    {
        numbers[i++] = trans_from_radix_to_dec(res, strlen(res), cbase);
        if (strcmp(numbers[i-1],"!") == 0)
            return -1;
        res = strtok (NULL, ",");
    }
    return i;
}

/************************************************
* Функция проверки на корректность цифр в числе *
*************************************************/
int check_base(char number[], int len, int radix)
{
    int i, num_curr;
    for (i = 0; i < len; i++)
    {    
        if (isdigit(*(number + i)) && ((*(number + i)) - '0' < radix))   continue; 

        if (isalpha(*(number + i)))    num_curr = toupper(*(number + i)) - 'A' + 10;
        
        else
        {
            printf("Error, invalid character in %s\n", number);
            return 0;
        }
        if (num_curr >= radix)
        {
            printf("Error, number greater than base\n");
            return 0;
        }
    }
    return 1;
}

/****************************************************
* Функция перевода числа в 10-ую систему счисления  *
*****************************************************/
char *trans_from_radix_to_dec(char number[], int len, char radix[])
{
    unsigned long long i;
    int sign = 1;
    char *answer = strdup(number);
    char *ci = calloc(5, sizeof(char));
    char *power = calloc(pow(len, len), sizeof(char));
    char *multi = calloc(pow(len, len), sizeof(char));
    char *sum = calloc(pow(len, len), sizeof(char));
    sum = "0";

    if (*answer == '-')
    {
        sign = -1;
        len--;
        delChar(answer, '-');
    }
    if(atoi(radix) >= 2 && atoi(radix) <= 36)
    {
        if (check_base(answer, len, atoi(radix)) == 1)
        {
            if (atoi(radix) == 10)
            {
                sum = number;
                goto CLEANUP;
            }
            for (i = 0; i < len; i++)
            {
                if (isalpha(*(answer + len - 1 - i)) != 0)
                {
                    snprintf(ci, 3, "%lld", i);
                    power = func_pow(radix, strlen(radix), ci, 2);
                    snprintf(ci, 3, "%d", (toupper(*(answer + len - 1 - i)) - 'A' + 10));
                    multi = func_multi(ci, 2, power, strlen(power));
                    sum = func_plus(multi, strlen(multi), sum, strlen(sum));
                }
                else
                { 
                    snprintf(ci, 3, "%lld", i);
                    power = func_pow(radix, strlen(radix), ci, 2);
                    snprintf(ci, 3, "%d", (toupper(*(answer + len - 1 - i)) - '0'));
                    multi = func_multi(ci, strlen(ci), power, strlen(power));
                    sum = func_plus(multi, strlen(multi), sum, strlen(sum));
                }
            }
        }
        else
        {
            sum = "!";
            goto CLEANUP; 
        }
    }
    else 
    {
        printf("Error, base outside [2; 36]\n");
        goto CLEANUP; 
        return "!";
    }

    if (sign == -1)
    {
        add_elem(sum, strlen(sum), 1, '-');
        goto CLEANUP; 
    }
    else 
        goto CLEANUP;
 
    CLEANUP:
        if (ci) free(ci);
        return sum;
}

/*********************************************************
* Функция перевода из 10-ой в исходную систему счисления *
**********************************************************/
char *trans_from_dec_to_radix(char number[], int len, char cradix[], int len_cradix)
{
    if (atoi(cradix) == 10) return number;
    int sign = 1, i = 0;
    if (*number == '-')
    {
        sign = -1;
        len--;
        delChar(number, '-');
    }
    char *answer = calloc(pow(len, len), sizeof(char));
    char *copy = strdup(number);
    int result = func_compar(number, len, cradix, len_cradix);
    int radix = atoi(cradix);
    if (result == 2) 
    {
        if (radix > 9 && atoi(number) > 9)
        {
            *answer = atoi(number) - 10 + 'A';
            if (sign == -1)
            {
                add_elem(answer, strlen(answer), 1, '-');
                return (answer);
            }
            else return (answer);
        }
        else
        {
            answer = strdup(number);
            if (sign == -1)
            {
                add_elem(answer, strlen(answer), 1, '-');
                return (answer);
            }
            else return (answer);
        }

    }
    else
    {
        char *str = calloc(pow(len, len), sizeof(char));
        char *letter = calloc(pow(len, len), sizeof(char));

        while ((len >= 1) && (*copy != '0'))
        {
            str = func_del(copy, strlen(copy), cradix, len_cradix);
            letter = func_mod(copy, strlen(copy), cradix, len_cradix);
            if (atoi(letter) > 9)
            {
                *letter = atoi(letter) - 10 + 'A';
                strncat(answer, letter, 1); 
            }
            else
            {
                strncat(answer, letter, 1); 
            }
            copy = strdup(str);
            len = strlen(str);
            if (str) free(str);
            if (letter) free(letter);
        }
    }
    copy = strdup(answer);
    len = strlen(answer);
    for (i = 0; i < len; i++)
    {
        delChar(answer, *answer);
    }
    for (i = strlen(copy); i >= 0; i--)
    {
        strncat(answer, (copy+i), 1); 
    }
    if (sign == -1)
    {
        add_elem(answer, strlen(answer), 1, '-');
        return (answer);
    }
    else return (answer);
}

/************************************
* Функция, объединяющая все функции  *
*************************************/
void func_com(size_t read, char *line)
{
    int num_count = 0, i = 0;
    char *cbase = calloc(3,sizeof(char));
    char *numbers = calloc(read, sizeof(read));
    char *operators = calloc(read,sizeof(read));
    char *number[sizeof(numbers)] = {0};
    int ans = parse(line, cbase, numbers, operators); 
    if (ans != 0)
        return;
    num_count = get_numbers(numbers, number, cbase);
    char *result = "0";
    if ((num_count <= 1) && isalnum(operators[0]) == 0)
    {
        printf("Sorry, try again\n");
        return;
    }
    result = number[0];
    while (i < num_count-1)
    {
        switch (operators[i]) 
        {
            case '+':
                result = func_plus(result, strlen(result), number[i+1], strlen(number[i+1]));
                break;
            case '-':
                result = func_minus(result, strlen(result), number[i+1], strlen(number[i+1]));
                break;
            case '*':
                result = func_multi(result, strlen(result), number[i+1], strlen(number[i+1]));
                break;
            case '/':
                result = func_del(result, strlen(result), number[i+1], strlen(number[i+1]));
                if (strcmp(result, "!") == 0) return;
                break;
            case '^':
                result = func_pow(result, strlen(result), number[i+1], strlen(number[i+1]));
                if (strcmp(result, "!") == 0) return;
                break;
            case '%':
                result = func_mod(result, strlen(result), number[i+1], strlen(number[i+1]));
                if (strcmp(result, "!") == 0) return;
                break;
            default:
                printf("Error, incorrect operators\n");
                return;
                break;
        result = trans_from_dec_to_radix(result, strlen(result), cbase, strlen(cbase));
        }
        i++;
    }
    result = trans_from_dec_to_radix(result, strlen(result), cbase, strlen(cbase));
    if (output_file == NULL)
        printf("Answer = %s\n", result);
    else
    { 
        fprintf(output_file, "Answer = %s\n", result);
        fflush(output_file);
    }
    if (cbase) free(cbase);
    if (numbers) free(numbers);
    if (operators) free(operators);
    if (result) free(result);
}
